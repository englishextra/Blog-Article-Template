# Blog-Article-Template

It's responsive and it uses REMs. It is readable and clean. It has
off-canvas navigation, to-top button, and it mostly uses CDN-hosted
scripts and styles. See inline CSS styles overrides and media queries.

[Demo][]

[Github][]


  ��[englishextra.github.com][], 2015

  [englishextra.github.com]: https://englishextra.github.com/
  [Demo]: https://englishextra.github.io/libs/Blog-Article-Template/
  [Github]: https://github.com/englishextra/Blog-Article-Template
